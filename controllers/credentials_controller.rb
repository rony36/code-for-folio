class CredentialsController < ApplicationController
  authorize_resource class: false
  
  before_action :credential_model
  before_action :set_credential, only: %i(edit update destroy)
  
  PERMITTED_ATTRIBUTES = {
      amadeus: %i(ipcc username password wsap),
      apollo: %i(username password branch_code ipcc),
      british_airways: %i(client_key sender_name sender_agency_id sender_iata_number sender_email),
      cardelmar: %i(broker agency_no user_email default_login default_partner_no default_customer_no
                  default_user_password),
      expedia: %i(cid api_key shared_secret currency_code minor_rev thumbnail_domain),
      farelogix: %i(system_identity_password ipcc agent_password agent_role agency_code agent_id),
      galileo: %i(username password branch_code ipcc),
      gateway: %i(requestor_type requestor requestor_message_password requestor_company_name
                requestor_company_code requestor_country_code),
      rentalcar: %i(username password),
      sabre: %i(client client_secret username password ipcc),
      travelport_rooms_and_more: %i(username password branch_code),
      worldspan: %i(username password branch_code ipcc),
  }.freeze
  
  ROUTING_ERROR = 'Invalid Credential Type'.freeze
  
  def index
    @credentials = credential_model.all
  end
  
  def new
    @credential = credential_model.new
  end
  
  def edit
  end
  
  def create
    @credential = credential_model.new(credential_params)
    render :new unless @credential.save
    redirect_to credentials_url(type: credential_type),
                notice: I18n.t('shared.created', resource: credential_model.model_name)
  end
  
  def update
    render :edit unless @credential.update(credential_params)
    redirect_to credentials_url(type: credential_type),
                notice: I18n.t('shared.updated', resource: credential_model.model_name)
  end
  
  def destroy
    return unless @credential.destroy
    redirect_to credentials_url(type: credential_type),
                notice: I18n.t('shared.deleted', resource: credential_model.model_name.human)
  end
  
  private
  
  def credential_type
    params[:type].to_sym
  end
  
  def credential_model
    fail ActionController::RoutingError, ROUTING_ERROR unless PERMITTED_ATTRIBUTES.include? credential_type
    "#{credential_type}_credential".classify.constantize
  end
  
  def set_credential
    @credential = credential_model.find(params[:id])
  end
  
  def credential_params
    params.
        require(credential_model.name.underscore.to_sym).
        permit(permitted_attributes).merge(tenant_id: current_tenant.id)
  end
  
  def permitted_attributes
    PERMITTED_ATTRIBUTES[credential_type]
  end
end
