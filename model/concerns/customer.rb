module Customer
  extend ActiveSupport::Concern
  include AuditedTimeline::AuditedConcern
  
  LANGUAGES = %i(de fr en).freeze
  
  included do
    audited
    has_associated_audits
    
    has_many :orders, foreign_key: :customer_id
    has_many :invoices, foreign_key: :customer_id
    has_many :messages, foreign_key: :customer_id
    has_many :comments, class_name: 'CustomerComment', foreign_key: :customer_id
    has_many :sms, class_name: 'SMS', foreign_key: :customer_id
    
    store_accessor :sti_store, :lsv_enabled
    store_accessor :sti_store, :bank_clearing_number
    store_accessor :sti_store, :bank_account_number
    store_accessor :sti_store, :credit
    store_accessor :sti_store, :title
    store_accessor :sti_store, :star_id
    
    store_accessor :sti_store, :customer_number
    store_accessor :sti_store, :correspondence_language
    store_accessor :sti_store, :mobile_number
    store_accessor :sti_store, :facebook_account
    store_accessor :sti_store, :twitter_account
    store_accessor :sti_store, :skype_account
    
    validates :bank_clearing_number, numericality: { only_integer: true }, if: :lsv_enabled?
    validates :bank_clearing_number, length: { minimum: 3, maximum: 5 }, if: :lsv_enabled?
    validates :bank_account_number, length: { minimum: 3, maximum: 34 }, if: :lsv_enabled?
    
    validates :correspondence_language, inclusion: LANGUAGES.map(&:to_s), allow_blank: true
    
    attr_accessor :type
    after_create :set_customer_number
    
    def credit
      value = super.nil? ? 0 : super
      BigDecimal.new(value.to_s)
    end
    
    def lsv_enabled
      !!ActiveRecord::Type::Boolean.new.type_cast_from_database(super)
    end
  end
  
  class_methods do
    def find_by_customer_number(customer_number)
      where("sti_store->'customer_number' = ?::varchar", customer_number).first
    end
    
    def accounts_client_class
      Accounts::Client::Customer
    end
  end
  
  def to_s
    "#{customer_number}: #{name}"
  end
  
  def credit=(value)
    super BigDecimal.new(value.to_s)
  end
  
  def lsv_enabled=(value)
    super !!ActiveRecord::Type::Boolean.new.type_cast_from_user(value)
  end
  
  def tour_online_identifier=(value)
    if value.to_s =~ /\A\d+\z/
      super format('%04d', value.to_i)
    else
      super value
    end
  end
  
  def set_customer_number
    update(customer_number: id) unless customer_number
  end
  
  def lsv_enabled?
    lsv_enabled
  end
  
  def specific_params_for_accounts
    {
        direct_debit_enabled: lsv_enabled?,
        bank_clearing_number: bank_clearing_number,
        bank_account_number: bank_account_number,
    }
  end
end
