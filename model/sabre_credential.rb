class SabreCredential < ActiveRecord::Base
  belongs_to :tenant
  
  validates :client, :client_secret, :username, :password, :ipcc, :tenant_id, presence: true
  validates :ipcc, uniqueness: true
  
  attr_encrypted :client, :client_secret, :username, :password,
                 key: Figaro.env.sabre_encryption_key, mode: :per_attribute_iv_and_salt
  
  def to_s
    ipcc
  end
end
