class AmadeusCredential < ActiveRecord::Base
  belongs_to :tenant
  
  validates :ipcc, :username, :password, :wsap, presence: true
  validates :ipcc, uniqueness: true
  
  attr_encrypted :username, :password, :wsap,
                 key: Figaro.env.amadeus_encryption_key, mode: :per_attribute_iv_and_salt
  
  def to_s
    ipcc
  end
end
