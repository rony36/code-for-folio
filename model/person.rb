class Person < Address
  include Customer
  
  MEALS = %i(standard vegetarian vegan muslim kosher diabetic).freeze
  SALUTATIONS = %i(mr mrs ms).freeze
  TITLE_PREFIXES = %i(dr prof dr_prof).freeze
  PREFERRED_COMMUNICATION = %i(phone email).freeze
  PREFERRED_SEATING = %i(window aisle).freeze
  PREFERRED_PAYMENT = %i(cash credit_card invoice).freeze
  
  has_many :frequent_flyer_numbers, foreign_key: :person_id
  belongs_to :organization
  has_and_belongs_to_many :interests, foreign_key: 'address_id'
  
  accepts_nested_attributes_for :frequent_flyer_numbers, allow_destroy: true
  
  store_accessor :sti_store, :first_name
  store_accessor :sti_store, :middle_name
  store_accessor :sti_store, :last_name
  store_accessor :sti_store, :salutation
  store_accessor :sti_store, :title_prefix
  store_accessor :sti_store, :meal
  store_accessor :sti_store, :emergency_number
  
  store_accessor :sti_store, :business_email
  store_accessor :sti_store, :business_telephone
  store_accessor :sti_store, :preferred_communication
  store_accessor :sti_store, :preferred_seating
  store_accessor :sti_store, :preferred_payment
  
  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :meal, inclusion: MEALS.map(&:to_s), allow_blank: true
  validates :salutation, inclusion: SALUTATIONS.map(&:to_s), allow_blank: true
  validates :title, inclusion: TITLE_PREFIXES.map(&:to_s), allow_blank: true
  validates :preferred_communication, inclusion: PREFERRED_COMMUNICATION.map(&:to_s), allow_blank: true
  validates :preferred_seating, inclusion: PREFERRED_SEATING.map(&:to_s), allow_blank: true
  validates :preferred_payment, inclusion: PREFERRED_PAYMENT.map(&:to_s), allow_blank: true
  
  before_validation :set_customer_name
  
  scope :birthday_at, lambda { |date|
    where("DATE_PART('day', birthday) = DATE_PART('day', :date::date)", date: date).
        where("DATE_PART('month', birthday) = DATE_PART('month', :date::date)", date: date)
  }
  
  def set_customer_name
    self.name = "#{first_name} #{last_name}"
  end
  
  def title_prefix_human
    I18n.t(title_prefix, scope: 'person.title_prefix') if title_prefix
  end
  
  def salutation_human
    I18n.t(salutation, scope: 'person.salutation')
  end
  
  def self.title_prefixes
    titles = {}
    TITLE_PREFIXES.each do |key|
      titles[I18n.t(key, scope: 'person.title_prefix')] = key
    end
    titles
  end
  
  def self.salutations
    salutations = {}
    SALUTATIONS.each do |key|
      salutations[I18n.t(key, scope: 'person.salutation')] = key
    end
    salutations
  end
end
