class Organization < Address
  include Customer
  
  has_many :persons, foreign_key: :organization_id
  has_and_belongs_to_many :interests, foreign_key: 'address_id'
  
  def self.search(str)
    addresses_by_tag = tagged_with(str)
    addresses_by_text = search_on_text_columns(str)
    (addresses_by_tag + addresses_by_text).uniq
  end
end
