class Address < ActiveRecord::Base
  CUSTOMER_CLASSES = %w(Organization Person).freeze
  
  acts_as_taggable
  
  validates :cets_identifier, uniqueness: true, allow_blank: true
  validates :tour_online_identifier, uniqueness: true, allow_blank: true
  validates :name, presence: true
  validates :email, presence: true, uniqueness: true
  
  before_create :create_in_accounts
  after_update :update_in_accounts
  after_destroy :delete_in_accounts
  
  scope :lsv_active, -> { customers.where("sti_store->'lsv_enabled' = 'true'") }
  scope :lsv_inactive,
        -> { customers.where("sti_store->'lsv_enabled' = 'false' OR NOT sti_store ? 'lsv_enabled'") }
  scope :customers, -> { where(type: CUSTOMER_CLASSES) }
  
  include PgSearch
  pg_search_scope :search_on_text_columns,
                  against: {
                      name: 'A',
                      phone: 'D',
                      line_1: 'B',
                      line_2: 'C',
                      zip_code: 'C',
                      city: 'B',
                  },
                  using: { tsearch: { prefix: true } }
  
  def self.search(str)
    addresses_by_tag = tagged_with(str)
    addresses_by_text = search_on_text_columns(str)
    (addresses_by_tag + addresses_by_text).uniq
  end
  
  def to_s
    name
  end
  
  def create_in_accounts
    address = self.class.accounts_client_class.new(params_for_accounts).create
    self.accounts_identifier = address.id
  end
  
  def update_in_accounts
    address = self.class.accounts_client_class.find(id: accounts_identifier)
    address.update_attributes(params_for_accounts)
    address.update
  end
  
  def delete_in_accounts
    address = self.class.accounts_client_class.find(id: accounts_identifier)
    address.destroy
  end
  
  def params_for_accounts
    specific_params_for_accounts.merge(
        name: name,
        street: line_1,
        zip: zip_code,
        city: city,
        extended_address: line_2,
        phone_numbers: [{ type: 'mobile', number: phone }]
    )
  end
  
  def specific_params_for_accounts
    raise 'must be implemented by subclass'
  end
  
  def self.accounts_client_class
    raise 'must be implemented by subclass'
  end
  
  def has_address?
    line_1.present? && zip_code.present? && city.present? && country_code.present?
  end
end
