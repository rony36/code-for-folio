RSpec.shared_examples 'Unauthorized and redirect to root' do |action, type|
  include_examples 'Perform request', action, type
  
  it 'should redirect to root' do
    expect(flash[:notice]).to eq('You are not authorized to access this page.')
    expect(response).to redirect_to :root
  end
end

RSpec.shared_examples 'Redirects with code' do |action, type, code|
  include_examples 'Perform request', action, type
  
  it "response code should be #{code}" do
    expect(response.code).to eq(code)
  end
end

RSpec.shared_examples 'Renders a template' do |action, type|
  include_examples 'Perform request', action, type
  
  it 'renders requested template successfully' do
    expect(response).to render_template(action)
  end
end

RSpec.shared_examples 'Assign an instance variable' do |action, type, variable, model|
  include_examples 'Perform request', action, type
  
  it 'should assign value in instance variable' do
    case action
      when :index
        expect(assigns(variable)).to eq tenant.public_send(model)
      when :new
        expect(assigns(variable).attributes).to eq "#{type}_credential".classify.constantize.new.attributes
      when :create, :update
        expect(assigns(variable)).to eq tenant.public_send(model).last
      else
        expect(assigns(variable)).to eq tenant.public_send(model).first
    end
  end
end

RSpec.shared_examples 'Redirect to credential listing' do |action, type|
  include_examples 'Perform request', action, type
  
  it 'redirect to credential listing' do
    expect(response).to redirect_to(credentials_url(type: type))
  end
end

RSpec.shared_examples 'Get logged in first as merchandiser' do
  before(:each) do
    sign_in user_merchandiser
  end
end

RSpec.shared_examples 'Get logged in first as technician' do
  before(:each) do
    sign_in user_technician
  end
end

RSpec.shared_examples 'Create provider credential' do
  before do
    provider_credential
  end
end

RSpec.shared_examples 'Perform request' do |action, type|
  include_examples 'Credential Attributes', type
  let(:perform_index) { get action, type: type }
  let(:perform_create) { post action, type: type, "#{type}_credential" => attributes }
  let(:perform_new) { get action, type: type }
  
  let(:perform_update) do
    put action, type: type, id: provider_credential.id, "#{type}_credential" => attributes
  end
  let(:perform_destroy) { delete action, type: type, id: provider_credential.id }
  let(:perform_edit) { get action, type: type, id: provider_credential.id }
  
  before do
    public_send "perform_#{action}"
  end
end

RSpec.shared_examples 'Credential Attributes' do |type|
  let(:british_airways_credential) do
    {
        client_key: 'CLIENT_KEY',
        sender_name: 'SENDER_NAME',
        sender_email: 'SENDER_EMAIL',
        sender_iata_number: 'SERDER_IATA_NUMBER',
        sender_agency_id: 'SENDER_AGENCY_ID',
    }
  end
  
  let(:cardelmar_credential) do
    {
        broker: '346543',
        agency_no: 'F536h556',
        user_email: 'someemail@gmail.com',
        default_login: 'someemail@gmail.com',
        default_partner_no: '3476346',
    }
  end
  
  let(:expedia_credential) do
    {
        cid: '587345',
        api_key: '48jc928347hnc89247287',
        shared_secret: 'dfw8475he',
        currency_code: 'USD',
        minor_rev: '26',
    }
  end
  
  let(:farelogix_credential) do
    {
        agent_id: 'agent_id',
        system_identity_password: 'iden_pass',
        ipcc: 'city_code',
        agent_password: 'agent_pass',
        agent_role: 'agent_role',
    }
  end
  
  let(:galileo_credential) do
    {
        requestor_type: '1',
        requestor: '46756',
        requestor_message_password: '457cn783n',
        requestor_company_name: 'Welltravel',
        requestor_company_code: '435745435',
    }
  end
  
  let(:attributes_without_tenant) do
    case type
      when :amadeus
        {
            ipcc: 'OFFICE',
            username: 'USER',
            password: 'PASSWORD',
            wsap: 'AmadeusWSAP',
        }
      when :apollo
        {
            username: 'USERNAME',
            ipcc: 'IPCC',
            password: 'PASSWORD',
            branch_code: 'BRANCH_CODE',
        }
      when :british_airways
        british_airways_credential.merge(participant_name: 'PARTICIPANT_NAME', participant_id: 'PARTICIPANT_ID')
      when :cardelmar
        cardelmar_credential.merge(default_customer_no: '6bh534f6', default_user_password: 'e564f345')
      when :expedia
        expedia_credential.merge(thumbnail_domain: 'test.welltravel.com')
      when :farelogix
        farelogix_credential.merge(agency_code: 'agency_code')
      when :galileo
        {
            username: 'fd9485msfggh6n',
            password: 'ghj6msfgn7',
            branch_code: 'P04538',
            ipcc: 'ipcc',
        }
      when :gateway
        galileo_credential.merge(requestor_country_code: 'CH')
      when :rentalcar
        {
            username: '46chn7846bvc',
            password: 'b34627cb83',
        }
      when :sabre
        {
            client: 'dhjkcuiy9r3423ddfgt',
            client_secret: 'h6tykceuei',
            username: '4475',
            password: '5bghy7rby7',
            ipcc: 'M85B',
        }
      when :travelport_rooms_and_more
        {
            username: 'username',
            password: 'password',
            branch_code: 'branch_code',
        }
      when :worldspan
        {
            username: 'WorldspanUsername',
            password: 'WorldspanPassword',
            branch_code: 'WorldspanIPCC',
            ipcc: 'WorldspanBranchCode',
        }
    end
  end
  
  let(:attributes) { attributes_without_tenant.merge(tenant_id: tenant.id) }
end

RSpec.shared_examples 'Creates a new record' do |type|
  include_examples 'Credential Attributes', type
  
  it "should create a #{type}_credential with parms" do
    expect do
      post :create, type: type, "#{type}_credential" => attributes
    end.to change { "#{type}_credential".classify.constantize.count }.from(1).to(2)
  end
end

RSpec.shared_examples 'Updates a field' do |type|
  include_examples 'Credential Attributes', type
  
  let(:ipcc_field_name) do
    if attributes.key?(:ipcc)
      'ipcc'
    else
      validators = provider_credential.class.validators
      unique_attributes = validators.detect do |validator|
        validator.class == ActiveRecord::Validations::UniquenessValidator
      end
      (unique_attributes || validators.first).attributes.first.to_s
    end
  end
  it "should update ipcc of #{type}_credential" do
    expect do
      put :update,
          type: type,
          id: provider_credential.id,
          "#{type}_credential" => { ipcc_field_name => 'new_ipcc' }
      provider_credential.reload
    end.to change { provider_credential.public_send(ipcc_field_name) }.to('new_ipcc')
  end
end

RSpec.shared_context 'Different Actions' do |action, type, assignee|
  describe "##{action}" do
    let(:provider_credential) { FactoryGirl.create("#{type}_credential".to_sym, tenant_id: tenant.id) }
    context 'when logged in as Technician' do
      let(:tenant) { user_technician.tenant }
      
      include_examples 'Create provider credential'
      include_examples 'Get logged in first as technician'
      
      case action
        when :create, :update
          context do
            include_examples 'Assign an instance variable', action, type, assignee, "#{type}_credentials".to_sym
          end
          context { include_examples 'Redirect to credential listing', action, type }
        when :destroy
          context { include_examples 'Redirect to credential listing', action, type }
        else
          context do
            include_examples 'Assign an instance variable', action, type, assignee, "#{type}_credentials".to_sym
          end
          context { include_examples 'Renders a template', action, type }
          context { include_examples 'Redirects with code', action, type, '200' }
      end
      
      include_examples 'Creates a new record', type if action == :create
      include_examples 'Updates a field', type if action == :update
    end
    
    context 'when logged out' do
      let(:tenant) { user_merchandiser.tenant }
      include_examples 'Redirects with code', action, type, '302'
    end
    
    context 'when logged in user is Merchandiser' do
      let(:tenant) { user_merchandiser.tenant }
      
      include_examples 'Create provider credential'
      
      include_examples 'Get logged in first as merchandiser'
      context { include_examples 'Unauthorized and redirect to root', action, type }
      context { include_examples 'Redirects with code', action, type, '302' }
    end
  end
end

RSpec.shared_context 'Provider Specific' do |type|
  context "#{type.capitalize} Credential" do
    include_context 'Different Actions', :index, type, :credentials
    include_context 'Different Actions', :create, type, :credential
    include_context 'Different Actions', :new, type, :credential
    include_context 'Different Actions', :update, type, :credential
    include_context 'Different Actions', :destroy, type, :credential
    include_context 'Different Actions', :edit, type, :credential
  end
end
