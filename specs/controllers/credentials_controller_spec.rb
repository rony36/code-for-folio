require 'spec_helper'

RSpec.describe CredentialsController, type: :controller do
  let(:user) { FactoryGirl.create(:user) }
  let(:user_technician) { FactoryGirl.create(:user, :technician) }
  let(:user_merchandiser) { FactoryGirl.create(:user, :merchandiser) }
  
  include_context 'Provider Specific', :amadeus
  include_context 'Provider Specific', :apollo
  include_context 'Provider Specific', :british_airways
  include_context 'Provider Specific', :cardelmar
  include_context 'Provider Specific', :expedia
  include_context 'Provider Specific', :farelogix
  include_context 'Provider Specific', :galileo
  include_context 'Provider Specific', :gateway
  include_context 'Provider Specific', :rentalcar
  include_context 'Provider Specific', :sabre
  include_context 'Provider Specific', :travelport_rooms_and_more
  include_context 'Provider Specific', :worldspan
end
